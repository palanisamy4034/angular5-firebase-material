import { HomeComponent } from './component/home/home/home.component';
import { LoginComponent } from './login/login.component';
import { SigninComponent } from './login/signin/signin.component';
import { Routes } from '@angular/router';
import { AuthGuard } from './guard/auth-guard.guard';
export const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'app-login',
        pathMatch: 'full'
    }, {
        path: 'app-signin',
        component: SigninComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'app-login',
        component: LoginComponent
    },
    {
        path: 'app-home',
        component: HomeComponent
    }
    // tslint:disable-next-line:eofline
];