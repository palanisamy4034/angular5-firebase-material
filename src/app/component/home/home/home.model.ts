

export class AWSS3 {
    Key: string;
    Type: string;
    prefix: string;
    LastModified: string;
    Size: number;
}
