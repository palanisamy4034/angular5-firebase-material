import { HomeService } from './home.service';
import { Component, OnInit } from '@angular/core';
import { AWSS3 } from './home.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  state: any;
  public fileDetail: AWSS3[] = [];
  previousfolder: string;
  loading: boolean = false;
  constructor(private hs: HomeService) { }

  ngOnInit() {
    this.hs.getIPInfo().subscribe(data => {
      console.log(data);
      this.state = data;
    });
    this.loading = !this.loading;

    this.hs.getAWSStorage().subscribe(obj => {
      this.fileDetail = obj.data;
      this.loading = !this.loading;

    });
  }

  SelectedFolder(folder: string, prefix?: string) {
    this.loading = !this.loading;
    const _prefix = prefix !== undefined ? prefix + '/' : '';
    const _temp = _prefix + folder;
    let split = _temp.split('/');
    this.previousfolder = split.slice(0, split.length - 1).join('/');
    this.hs.getAWSStorage(_temp).subscribe(obj => {
      this.loading = !this.loading;
      this.fileDetail = obj.data;
    });
  }

}
