import { Injectable } from '@angular/core';



import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class HomeService {

  public headers: Headers;
  ipInfoUrl: any = 'http://ip-api.com/json';

  constructor(private http: Http) {
    this.headers = new Headers();
    this.headers.append('Access-Control-Allow-Headers', 'Content-Type');
    this.headers.append('Access-Control-Allow-Methods', 'GET, POST');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }
  private extractedData(res: Response) {
    const body = res.json();
    return JSON.stringify(body || {});
  }

  public getIPInfo(): Observable<any> {
    return this.http
      .get(this.ipInfoUrl)
      .map((res: Response) => this.extractData(res))
      .catch((err: Response) => this.handleError(err));
  }

  public getAWSStorage(_temp?: string): Observable<any> {
    let _params = new URLSearchParams();
    _temp != '' && _temp != undefined ? _params.set('prev', _temp) : '';
    let endpoint = 'http://10.0.0.145:3000/validate';
    let options = new RequestOptions({ params: _params });
    return this.http
      .get(endpoint, options)
      .map((res: Response) => this.extractData(res))
      .catch((err: Response) => this.handleError(err));
  }


}
