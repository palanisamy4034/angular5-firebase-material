import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatProgressSpinnerModule } from '@angular/material';

@NgModule({
  exports: [MatButtonModule, MatProgressSpinnerModule],
})
export class MaterialModule { }
