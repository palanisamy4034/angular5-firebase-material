import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { FirebaseAuth } from 'firebase/auth';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/take';

@Injectable()
export class AuthGuard implements CanActivate {
  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //   return true;
  // }


  private user: Observable<firebase.User>;
  private userDetails: firebase.User;

  constructor(private auth: FirebaseAuth, private router: Router) { }

  canActivate(): Observable<boolean> {
    return this.auth.take(1)
      .map((authState: firebase.User) => !!authState)
      .do(authenticated => {
        if (!authenticated) {
          this.router.navigate(['/login']);
        }
      });
  }

}
