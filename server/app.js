const express = require('express');
const app = express();
var AWS = require('aws-sdk');
var uuid = require('node-uuid');
var router = express.Router();

// AWS.config.update({
//   accessKeyId: "<Access Key Here>",
//   secretAccessKey: "<Secret Access Key Here>"
// });

var s3 = new AWS.S3();

router.use(function (req, res, next) {
  console.log('Something is happening.');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});



router.get('/', function (req, res) {
  let _req = req.query;
  var content = {};
  let _prefix = _req.prev != '' ? _req.prev : '';
  s3.listObjects({
    Bucket: 'devtestloyalty',
    Prefix: _prefix
  }).on('success', function handlePage(response) {
    var resp = response.data;
    content = resp.Contents;
    console.log(content)
    var _res = awsStructure(content, _prefix)
    if (response.hasNextPage()) {
      response.nextPage().on('success', handlePage).send();
    }
    res.json({
      staus: 'sucesss',
      data: _res
    });
  }).send();

});


function awsStructure(content, _prefix) {
  var obj = [];
  for (let data of content) {
    let obj_type = data.Key;
    let _temp0bj = {};
    obj_type = obj_type.replace(_prefix + '/', '');
    if (obj_type.split('/').length > 1) {
      _temp0bj = {
        Key: obj_type.split('/')[0],
        Type: 'Folder',
        prefix: _prefix,
        LastModified: data.LastModified,
        Size: data.Size
      };
    } else {
      _temp0bj = {
        Key: obj_type,
        Type: 'File',
        prefix: _prefix,
        LastModified: data.LastModified,
        Size: data.Size
      };
    }
    obj.push(_temp0bj);
  }
  const unique_numbers = removeDuplicates(obj, 'Key');
  return unique_numbers;
}


function removeDuplicates(myArr, prop) {
  return myArr.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
  });
}


app.use('/validate', router);

module.exports = app;
