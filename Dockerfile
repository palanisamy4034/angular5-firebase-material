FROM node:8.9.1
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev vim
ENV NODE_ROOT /usr/app/
RUN mkdir -p $NODE_ROOT
WORKDIR $NODE_ROOT
COPY . .
RUN yarn install
RUN npm install -g @angular/cli
EXPOSE 4500 49153 
